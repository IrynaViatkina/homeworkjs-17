$(document).ready(function(){
	$('#btn').click(function(){
		$('#btn').remove();
		$('body').append('<div><p>Введите диаметр круга</p><input id="diameter" placeholder="0"></input></div>');
		$('body').append('<div><p>Введите цвет круга</p><input id="color" placeholder="имя цвета, RGB, HEX, HSL"></input></div>');
		$('<button>')
			.attr('id', 'draw')
			.attr('class', 'btn')
			.text("Нарисовать")
			.click(function(){
				$('<div>')
				.css({
					width: $('#diameter').val() + 'px',
					height: $('#diameter').val() + 'px',
					borderRadius: '50%',
					backgroundColor: $('#color').val()
				})
				.appendTo('body');
			})
		.appendTo('body');
	});
});