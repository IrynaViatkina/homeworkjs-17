$(document).ready(function(){
	$('#btn').click(function(){
		$('#btn').remove();
		$('body').append('<div><p>Введите диаметр круга</p><input id="diameter" placeholder="0"></input></div>');
		$('<button>')
			.attr('id', 'draw')
			.attr('class', 'btn')
			.text("Нарисовать")
			.click(function(){
				drawCircles($('#diameter').val())
			})
		.appendTo('body');
	});
});

function drawCircles(diameter){
	for(let i = 0; i < 10; i++){
		$('body').append('<div/>');
		for(let j = 0; j < 10; j++){
			$('<div>')
			.attr('class', 'circle')
			.css({
				display: 'inline-block',
				width: diameter + 'px',
				height: diameter + 'px',
				borderRadius: '50%',
				backgroundColor: `rgb(${Math.floor(Math.random() * 256)}, ${Math.floor(Math.random() * 256)}, ${Math.floor(Math.random() * 256)})`
			})
			//Ира, если в данном случае считается, что на каждый круг стоит свой отдельный обработчик,
			//а, согласно заданию, нужно написать какой-то общий обработчик для всех кругов, то я переделаю.
			.click(function(){
				$(this).hide();
			})
			.appendTo('body');
		}
	}
}